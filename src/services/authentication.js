import axios from 'axios'
import { BehaviorSubject } from 'rxjs';

const currentUserSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser')));

const signUp = async (userData) => {
    let user = await axios({
        method: 'post',
        url: '/api/v1/signup',
        data: userData,
    });
    localStorage.setItem('currentUser', JSON.stringify({ user: user.data }));
    currentUserSubject.next(user.data);
    return user.data;
}

const login = async (userData) => {
    let user = await axios({
        method: 'post',
        url: '/api/v1/login',
        data: userData
    });
    localStorage.setItem('currentUser', JSON.stringify({ user: user.data, rememberMe: userData.remember }));
    currentUserSubject.next(user.data);
    return user.data;
}

export const authenticationService = {
    signUp,
    login,
    currentUser: currentUserSubject.asObservable(),
    get currentUserValue() { return currentUserSubject.value }
}