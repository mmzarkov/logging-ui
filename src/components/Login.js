import React, { Component } from "react";
import { Form, Icon, Input, Button, Checkbox, Row, Divider, notification } from 'antd';
import Cookies from 'universal-cookie';
import Facebook from './Facebook';
import Google from './Google';
import { Link } from "react-router-dom";
import { authenticationService } from '../services/authentication';

class Login extends Component {

  handleSubmit = async e => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (err) {
        return;
      }
      try {
        let user = await authenticationService.login(values);
        notification.success({
          message: 'You have logged in successfully.',
        });

        const cookies = new Cookies();
        cookies.set('authn', user.token, { path: '/' });
        console.log(user.token);
        window.location.href = 'http://platform.test.alcatraz.ai/';
      } catch (e) {
        notification.error({
          message: 'Something went wrong. Please try again.',
        });
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Form onSubmit={this.handleSubmit} className="login-form">
          {/* username */}
          <Form.Item>
            {getFieldDecorator('username', {
              rules: [{ required: true, message: 'Please input your username!' }],
            })(
              <Input
                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="Username"
              />
            )}
          </Form.Item>

          {/* password */}
          <Form.Item>
            {getFieldDecorator('password', {
              rules: [{ required: true, message: 'Please input your Password!' }],
            })(
              <Input
                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                type="password"
                placeholder="Password"
              />
            )}
          </Form.Item>

          {/* remember */}
          <Form.Item>
            {getFieldDecorator('remember', {
              valuePropName: 'checked',
              initialValue: true,
            })(<Checkbox>Remember me</Checkbox>)}
            <Button type="primary" htmlType="submit" className="login-form-button" style={{ textTransform: "uppercase" }}>
              Log in
            </Button>
          </Form.Item>
        </Form>

        <Divider>Or</Divider>
        <Row type="flex" justify="center">
          <Facebook />
          <Google />
        </Row>

        <Divider />
        <Row type="flex" justify="center">
          <p>Don't have an account? <Link className="login-form-forgot" to={"/signup"}>Sign up</Link></p>
        </Row>
        <Divider />
      </div>
    );
  }
}

const LoginForm = Form.create({ name: 'login' })(Login);

export default LoginForm