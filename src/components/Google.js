import React, { Component } from 'react';
import GoogleLogin from 'react-google-login';

class Google extends Component {

    render() {

        const responseGoogle = (response) => {
            console.log(response);
        }

        return (
            <div>
                <GoogleLogin
                    clientId="473628710521-a4ulsa3f4hcn3o2beka3dkf21dmcgrl3.apps.googleusercontent.com"
                    buttonText="Login with Google"
                    onSuccess={responseGoogle}
                    onFailure={responseGoogle}
                />
            </div>
        );
    }
}

export default Google;
