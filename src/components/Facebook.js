import React, { Component } from "react";
import FacebookLogin from "react-facebook-login";

class Facebook extends Component {

  constructor() {
    super();
    this.state = {
      isLoggedIn: false,
      userID: "",
      name: "",
      email: ""
    };
  }

  responseFacebook = response => {
    this.setState({
      isLoggedIn: true,
      userID: response.userID,
      name: response.name,
      email: response.email
    });
  };

  render() {
    let fbContent;

    if (this.state.isLoggedIn) {
      fbContent = (
        <div>
          <h2>Welcome {this.state.name}</h2>
          Email: {this.state.email}
        </div>
      );
    } else {
      fbContent = (
        <FacebookLogin
          appId="1375760829361748"
          autoLoad={false}
          fields="name,email,picture"
          icon="fa-facebook"
          callback={this.responseFacebook}
          redirectUri="http://localhost:8181/oauth/facebook?redirect_uri=http://localhost:6969/api/v1/organizations"
        />
      );
    }

    return <div style={{ marginRight: "20px" }}>{fbContent}</div>;
  }
}

export default Facebook