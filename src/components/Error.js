import React, { Component } from "react";

class Error extends Component {
    render() {
        return <div>Ooops, something went wrong!</div>
    }
}

export default Error
