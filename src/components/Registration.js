import React, { Component } from "react";
import { Link } from "react-router-dom";
import Cookies from 'universal-cookie';
import Facebook from './Facebook';
import Google from './Google';
import { authenticationService } from '../services/authentication';
import {
  Form,
  Input,
  Tooltip,
  Icon,
  Button,
  Divider,
  Row,
  notification
} from 'antd';


class Registration extends Component {

  state = {
    confirmDirty: false
  };

  handleSubmit = async e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll(async (err, values) => {
      if (!err) {
        try {
          let user = await authenticationService.signUp(values);
          notification.success({
            message: 'You have registered successfully.',
          });

          const cookies = new Cookies();
          cookies.set('authn', user.token, { path: '/' });
          window.location.href = 'http://platform.test.alcatraz.ai/';
        } catch (e) {
          notification.error({
            message: 'Something went wrong. Please try again.',
          });
        }
      }
    });
  };

  handleConfirmBlur = e => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <div>
        <Row type="flex" justify="center">
          <div style={{ padding: "15px" }}>Sign up with your email address</div>
        </Row>
        <Form onSubmit={this.handleSubmit}>
          {/* firstName */}
          <Form.Item>
            {getFieldDecorator('firstName', {
              rules: [{ required: true, message: 'Please enter your first name!', whitespace: true }],
            })(<Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="First name"
            />)}
          </Form.Item>

          {/* lastName */}
          <Form.Item>
            {getFieldDecorator('lastName', {
              rules: [{ required: true, message: 'Please enter your lastname!', whitespace: true }],
            })(<Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Last name"
            />)}
          </Form.Item>

          {/* email */}
          <Form.Item>
            {getFieldDecorator('email', {
              rules: [
                {
                  type: 'email',
                  message: 'The input is not valid e-mail address!',
                },
                {
                  required: true,
                  message: 'Please enter your e-mail address!',
                },
              ],
            })(<Input
              prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Email"
            />)}
          </Form.Item>

          {/* username */}
          <Form.Item>
            {getFieldDecorator('username', {
              rules: [{ required: true, message: 'Please enter your username!', whitespace: true }],
            })(<Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Username"
            />)}
          </Form.Item>

          {/* password */}
          <Form.Item hasFeedback>
            {getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                  message: 'Please enter your password!',
                },
                {
                  validator: this.validateToNextPassword,
                },
              ],
            })(<Input
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
              placeholder="Password"
              suffix={
                <Tooltip title="Extra information">
                  <Icon type="info-circle" style={{ color: 'rgba(0,0,0,.45)' }} />
                </Tooltip>
              }
            />)}
          </Form.Item>

          {/* re-type password */}
          <Form.Item hasFeedback>
            {getFieldDecorator('confirm', {
              rules: [
                {
                  required: true,
                  message: 'Please confirm your password!',
                },
                {
                  validator: this.compareToFirstPassword,
                },
              ],
            })(<Input
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
              placeholder="Confirm password"
              onBlur={this.handleConfirmBlur}
              suffix={
                <Tooltip title="Extra information">
                  <Icon type="info-circle" style={{ color: 'rgba(0,0,0,.45)' }} />
                </Tooltip>
              }
            />)}
          </Form.Item>

          {/* register */}
          <Form.Item>
            <Button type="primary" htmlType="submit" className="login-form-button" style={{ textTransform: "uppercase" }}>
              Register
            </Button>
          </Form.Item>
        </Form>

        <Divider>Or</Divider>
        <Row type="flex" justify="center">
          <Facebook />
          <Google />
        </Row>

        <Divider />
        <Row type="flex" justify="center">
          <p>Already have an account? <Link className="login-form-forgot" to={"/"} >Log in</Link></p>
        </Row>
        <Divider />
      </div>
    );
  }
}

const RegistrationForm = Form.create({ name: 'register' })(Registration);

export default RegistrationForm