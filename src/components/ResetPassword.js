import React, { Component } from "react";
import { Form, Icon, Input, Button, Row, Divider } from 'antd';
import { Link } from "react-router-dom";


class ResetPassword extends Component {

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div>
                <Divider />
                <Row style={{ textAlign: "center" }}>
                    <div style={{ padding: "5px" }}>Enter your <strong>Alcatraz Ai username</strong> that you used to register.</div>
                    <div style={{ padding: "15px" }}>We'll send you an email with your username and a link to reset your password.</div>
                </Row>
                <Form onSubmit={this.handleSubmit} >
                    <Form.Item>
                        {getFieldDecorator('username', {
                            rules: [{ required: true, message: 'Please input your username!' }],
                        })(<Input
                            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="Username"
                        />)}
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button" style={{textTransform: "uppercase"}}>
                            Reset
                        </Button>
                    </Form.Item>
                </Form>
                <Divider />
                <Row type="flex" justify="center">
                    <p>If you still need help, contact<Link className="login-form-forgot" to={"/"}> Alcatraz AI support</Link></p>
                </Row>
                <Divider />
            </div>
        );
    }
}

const ResetPasswordForm = Form.create({ name: 'login' })(ResetPassword);

export default ResetPasswordForm