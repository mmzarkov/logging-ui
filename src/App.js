import React from "react";
import { Layout, Row } from 'antd';
import LoginForm from './components/Login';
import RegistrationForm from './components/Registration';
import ResetPassword from './components/ResetPassword';
import Error from './components/Error';
import logo from "./styles/logo.png";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

const { Header, Content } = Layout;

class App extends React.Component {

  render() {
    return <Router><Layout>
      <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }} />
      <Content style={{ padding: '0 50px', marginTop: 64 }}>
        <Row type="flex" justify="center" >
          <div style={{ background: '#fff', padding: 24, minHeight: 380 }}>
            <div className="logo" style={{ marginBottom: "50px" }}>
              <img src={logo} alt="Alcatraz.ai" />
            </div>
            <Switch>
              <Route exact path='/' component={LoginForm}/>
              <Route path="/signup" component={RegistrationForm} />
              <Route path="/forgot-password" component={ResetPassword} />
              <Route component={Error} />
            </Switch>
          </div>
        </Row>
      </Content>
    </Layout></Router>
  }
}

export default App
